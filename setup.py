#!/usr/bin/env python

from setuptools import setup
from iopdflib import __ver__

long_description = open("README.md", "r").read()
    
setup(name='iopdflib',
    version=__ver__,
    description='PDF parsing library (+ iopdf.py tool).',
    long_description=long_description,
    long_description_content_type="text/markdown",
    author='Vlad Topan',
    author_email='vtopan/gmail',
    url='https://gitlab.com/vtopan/iopdflib',
    license='MIT License',
    packages=['iopdflib'],
    classifiers = [
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent", 
        "License :: OSI Approved :: MIT License"
        ],
    install_requires=[]
)