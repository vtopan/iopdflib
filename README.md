# iopdflib

PDF parsing library (+ `iopdf.py` tool). Author: Vlad Topan.


## Purpose

- malware analysis
- text extraction for NLP
- conversion to HTML


## Status

Alpha version:
- parsing works (not complete)
- text-only HTML generation works as prototype
- decoders are implemented but not exposed in the tool (for malware analysis)


## Feedback

Feedback is welcome @ vtopan/gmail or as tickets here.
