"""
Generic data reader.

Author: Vlad Ioan (vtopan/gmail)
"""
import mmap
import os
import re


RX = {
    'crlf': (rb'\r\n?|\n',),
    }
for k in RX:
    RX[k] = re.compile(*RX[k])


class DataSource:
    """
    Data source.

    The current position is `.pos`, the last read character is in `.last`.

    :param data: Open file handle, raw binary data or None.
    :param filename: The source's file name (must be set if data is None).
    :param seek: Seek to this offset in the buffer / file before starting.
    """

    def __init__(self, filename=None, data=None, seek=0):
        self.fh = self.raw_data = None
        self.filename = filename
        self.size = None
        if not data:
            if not filename:
                raise ValueError('data and filename can\'t both be empty/None!')
        elif type(data) in (bytes, bytearray):
            self.raw_data = data
        elif hasattr(data, 'read'):
            self.fh = data
        self.next_pos = seek
        self.last = None
        self.size = len(self.raw_data) if self.raw_data else os.path.getsize(self.filename)


    def read(self, offs=None, count=None, peek=False):
        """
        Return raw bytes from the file/data.

        :param peek: Current pointer moves if this is False and `count` is set.
        """
        # prin(f'READ: @{offs}[{count}], peek={peek}')
        res = self.data
        if offs:
            if count:
                res = res[offs:offs + count]
            else:
                res = res[offs:]
        elif count:
            res = res[:count]
        if not peek:
            if count:
                self.next_pos = (offs or 0) + count
            self.last = res[-1:]
        return res


    def next(self, count=1, peek=False):
        """
        Returns the next byte(s) in the buffer.

        :param count: Number of bytes to read.
        :param peek: If set, don't move current pointer.
        :return: None if not enough bytes, data as `bytes` otherwise.
        """
        if self.next_pos + count > self.size:
            return None
        return self.read(self.next_pos, count=count, peek=peek)


    def peek(self, count=1):
        """
        Returns the next byte(s) (as `bytes`) in the buffer without moving current pointer.
        """
        return self.next(count=count, peek=True)


    def seek(self, pos):
        """
        Jump to position.
        """
        self.next_pos = pos
        self.last = self.data[pos - 1:pos] if pos > 0 else None


    def back(self, count=1):
        """
        Seeks back `count` bytes in the buffer.
        """
        self.next_pos -= count
        pos = self.next_pos - 1
        self.last = self.data[pos - 1:pos] if pos > 0 else None


    def readline(self, peek=False):
        """
        Read up to (and including) the next LF (b'\n').

        :return: None if no more data is available, or the line (including trailing b'\n').
        """
        m = RX['crlf'].search(self.data, pos=self.next_pos)
        if m:
            res = self.data[self.next_pos:m.end()]
            next_pos = m.end()
        else:
            return None
        if not peek:
            self.next_pos = next_pos
        return res


    @property
    def data(self):
        """
        Acces the data from the file/buffer directly.
        """
        if not self.raw_data:
            if not self.fh:
                self.fh = open(self.filename, 'rb')
            self.raw_data = mmap.mmap(self.fh.fileno(), 0, access=mmap.ACCESS_READ)
        return self.raw_data


    def __len__(self):
        return self.size


    def __getitem__(self, pos):
        if type(pos) is slice:
            start = pos.start or 0
            stop = pos.stop or self.size
            return self.read(start, count=stop - start)
        return self.read(pos, count=1)


    @property
    def handle(self):
        """
        Internal file handle (if any).
        """
        return self.fh

