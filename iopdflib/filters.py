"""
iopdf: filter handling.

Notes:
- DCTDecode buffers are raw JPG images.
- CCITTFaxDecode is a TIFF without the header
    - see https://stackoverflow.com/questions/2641770/extracting-image-from-pdf-with-ccittfaxdecode-filter#34555343
- JBIG2Decode has a sample header in sec. 3.3.6, p.82
- JPXDecode contains a file header - save as .jpf/.jpx ?

todo: test cases for each decoder
todo: JBIG2Decode (convert/save as .jbig2?), DCTDecode, CCITTFaxDecode, JPXDecode

Author: Vlad Topan (vtopan/gmail)
"""
import base64
import binascii
import re
import zlib


LZW_BASE_TABLE = [bytes([i]) for i in range(256)] + [None] * (4096 - 256)
LZW_CLEAR = 256
LZW_STOP = 257
LZW_NBITS = {511:10, 1023:11, 2047:12}

ABBREVIATIONS = {
    # per H.3.3 "Abbreviations for standard filter names" @ p.1100
    'AHx': 'ASCIIHexDecode',
    'A85': 'ASCII85Decode',
    'LZW': 'LZWDecode',
    'Fl': 'FlateDecode',
    'RL': 'RunLengthDecode',
    'CCF': 'CCITTFaxDecode',
    'DCT': 'DCTDecode',
    }

RX = {
    'whitespace': (rb'[\r\n\t ]+',),
    }
for k in RX:
    RX[k] = re.compile(*RX[k])


def ASCIIHexDecode(data):
    """
    Decode ASCII hex-encoded data.
    """
    data = RX['whitespace'].sub(b'', data)
    data = data.rstrip(b'>')
    if len(data) % 2:
        data += b'0'
    return binascii.unhexlify(data)


def ASCII85Decode(data):
    """
    Decode base-85 encoded data.
    """
    data = RX['whitespace'].sub(b'', data)
    if data.endswith(b'~>'):
        data = data[:-2]
    data = base64.a85decode(data)
    return data


def FlateDecode(data):
    """
    Flate decoding (zlib/deflate).
    """
    # data = zlib.decompress(data)  # trailing characters break this?
    try:
        data = zlib.decompressobj().decompress(data)
    except zlib.error as e:
        cm = data and (data[0] & 0xF)
        if cm and cm != 8:
            raise ValueError(f'Unknown ZLIB compression method {cm} (should be 8 = DEFLATE)!') from e
        raise e
    return data


def LZWDecode(data):
    """
    LZW decoding.
    """
    table, tsize, nbits, prev = LZW_BASE_TABLE[:], 258, 9, b''
    pos = bleft = 0
    res = []
    while 1:
        needed = nbits
        # read next code
        code = 0
        while needed:
            # bits still needed for the code
            if not bleft:
                # need new char from buffer
                if pos >= len(data):
                    # fixme: raise error if code != 0?
                    break
                byte = data[pos]
                pos += 1
                bleft = 8
            # compute how many bits can be taken
            get = min(needed, bleft)
            needed -= get
            bleft -= get
            code = (code << get) | (byte >> (8 - get))
            byte = (byte << get) & 0xFF
        if code == LZW_CLEAR:
            table, tsize, nbits, prev = LZW_BASE_TABLE[:], 258, 9, b''
            continue
        if code == LZW_STOP:
            break
        if not prev:
            c = prev = table[code]
        else:
            if code < tsize:
                c = table[code]
                table[tsize] = prev + c[0:1]
            elif code == tsize:
                table[tsize] = prev + prev[0:1]
                c = table[code]
            else:
                raise ValueError(f'Invalid code {code} (table size = {tsize})!')
            tsize += 1
            nbits = LZW_NBITS.get(tsize, nbits)
            prev = c
        res.append(c)
    return b''.join(res)


def RunLengthDecode(data):
    """
    Decode run-length encoded data.
    """
    res = []
    pos = 0
    while pos < len(data):
        c = data[pos]
        pos += 1
        if 0 <= c <= 127:
            res.append(data[pos:pos + c])
            pos += c
        elif c == 128:
            break
        else:
            res.append(data[pos:pos + 1] * (257 - c))
            pos += 1
    return b''.join(res)


