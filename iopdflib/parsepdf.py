#!/usr/bin/env python3
"""
iopdf: PDF objects.

Author: Vlad Topan (vtopan/gmail)
"""

from .datasource import DataSource
from . import filters as flt

import ast
import binascii
import re
import sys


RX = {
    'magic': (rb'%PDF-(1\.\d)',),
    'id': (rb'(\d+)\s+(\d+)\s+(obj|R)',),
    'number': (rb'[+-]?\d*(\.\d*)?',),
    'stream': (rb'stream\r?\n(.+?)\r?\n?endstream\s*', re.S),
    'bool': (rb'true|false',),
    'string': (rb'\)|.*?(?:(?:[^\\]|\\\\)\))', re.S),
    'string-inner': (rb'(^|[^\\])\(',),
    'hexstring': (rb'<[a-f0-9 ]*>', re.I),
    'intpair': (rb'(\d+)\s+(\d+)',),
    'tokenizer': (r'(<[\da-fA-F]+>|<<|>>|[\[\]{}<>%]|\s+|/[^\[\](){}<>%/\s]+|\([^)]+\))',),
    'is-number': (r'^[-+]?\d*\.?\d*$',),
    'octals': (r'\\([0-7]{1,3})',),
}
for k in RX:
    RX[k] = re.compile(*RX[k])

EOF = b'%EOF'
WHITESPACE = b' \r\t\n\0'
TEXT_DELIMITERS = set('[](){}<>') | {'<<', '>>'}



class PdfObj:
    """
    PDF object or element.
    """

    def __init__(self, doc, offs=None, oid=None, gen=None, value=None, desc=None, size=None,
            parent=None, children=None, in_stream=False, toplevel=False):
        self.stream = None
        self.doc = doc
        self.ds = doc.ds
        self.objtype = self.__class__.__name__[2:].lower()
        self.value = value
        self.desc = desc
        self.toplevel = toplevel
        if offs is None:
            offs = self.ds.next_pos - 1
        self.offs = offs
        self.parent = parent
        self.children = children if children is not None else []
        self.in_stream = in_stream  # if not None, the ID of the StmObj which contains the object
        if oid is not None:
            self.set_oid(oid, gen)
        if not size:
            size = self.parse()
        self.set_size(size)
        self.doc.objlst.append(self)


    def set_oid(self, oid, gen):
        self.oid = int(oid)
        self.gen = int(gen)
        if self.oid in self.doc.objects and self.gen < self.doc.objects[self.oid].gen:
            # older generation than known object, ignore
            return
        self.doc.objects[self.oid] = self


    def set_size(self, size):
        self.size = size
        self.data = self.doc.ds[self.offs:self.offs + self.size]


    def is_reference(self):
        return self.objtype == 'reference'


    @property
    def dereferenced(self):
        """
        Returns the dereferenced object (self if the object is not a reference).
        """
        crt = self
        while crt.is_reference():
            crt = self.doc.objects[crt.value]
        return crt


    @property
    def dvalue(self):
        """
        Return the element's dereferenced value (if the element is a reference, follow it).
        """
        obj = self
        while obj.is_reference() or getattr(obj.value, 'objtype', None) == 'reference':
            obj = obj.doc.objects[obj.value if obj.objtype == 'reference' else obj.value.value]
        return obj.value


    @property
    def text(self):
        """
        Render the value as text.
        """
        return str(self.value if self.value is not None else self.desc)


    def __repr__(self):
        return f'<{self.objtype}:{self.text}@{self.offs}[{self.size}]>'


class POComment(PdfObj):
    """
    Comment object.
    """


class PONull(PdfObj):
    """
    Null object.
    """


class POBoolean(PdfObj):
    """
    Boolean object ('true' or 'false').
    """


class PONumber(PdfObj):
    """
    Integer or real number object.
    """


class POName(PdfObj):
    """
    Name object ('/...').
    """

    def parse(self):
        res = []
        while self.ds.next() not in WHITESPACE + b'[]<>(){}/%':
            c = chr(int(self.ds.next(2), 16) if self.ds.last == b'#' else ord(self.ds.last))
            res.append(c)
        self.value = ''.join(res)
        # can be empty (i.e. a single "/") - see 3.2.4 "Named Objects", p.57
        return self.ds.next_pos - 1 - self.offs


class POString(PdfObj):
    """
    String object.
    """

    def parse(self):
        self._text = None
        data = self.ds.data
        fc = data[self.offs:self.offs + 1]
        if fc == b'(':
            self.stype = '('
            open_para = 0
            buf = b''
            offs = self.offs + 1
            while 1:
                crt = RX['string'].search(data, pos=offs).group()
                buf += crt
                offs += len(crt)
                open_para += len(RX['string-inner'].findall(crt))
                if not open_para:
                    break
                open_para -= 1
            self.value = buf[:-1]
            # fixme: replace '\\\r?\n' with nothing (ref. p.55)
            # fixme: replace '\\[rntbf()\\]' and '\ddd' / `\dd` / `\d` with the appropriate chars
            return len(self.value) + 2
        elif fc == b'<':
            self.stype = '<'
            self.value = RX['hexstring'].search(data, pos=self.offs).group()[1:-1]
            size = len(self.value) + 2
            self.value = self.value.replace(b' ', b'')
            if len(self.value) % 2:
                # 0-pad odd length hex strings (per "Hexadecimal Strings", p.56)
                self.value += b'0'
            self.value = binascii.unhexlify(self.value)
            return size
        else:
            raise ValueError(f'Invalid string start {fc}!')


    @property
    def text(self):
        """
        Return the value as text (if possible, None otherwise).
        """
        if not self._text:
            codec = 'utf16' if self.value.startswith(b'\xFE\xFF') else 'utf8'
            try:
                self._text = self.value.decode(codec)
            except UnicodeDecodeError:
                self._text = None
        return self._text


class POArray(PdfObj):
    """
    Array object ('[ ... ]').
    """

    def parse(self):
        lst = []
        while 1:
            self.doc.goto_next_char(True)
            if self.ds.last == b']':
                break
            self.ds.back()
            e = self.doc.parse_object()
            if e.objtype != 'comment':
                self.doc.dbg(f'ELEMENT: {e}', 4)
                lst.append(e)
        self.value = lst
        return self.ds.next_pos - self.offs


    def __getitem__(self, k):
        return self.dvalue[k]


class PODictionary(PdfObj):
    """
    Dictionary object ('/Name Value').
    """

    def parse(self):
        d = {}
        self.Type = None
        while 1:
            self.doc.goto_next_char(True)
            if self.ds.last == b'>':
                nchar = self.ds.next()
                if nchar != b'>':
                    raise ValueError(f'Invalid dictionary end: {nchar}!')
                break
            k = POName(self.doc)
            self.doc.dbg(f'KEY: {k} {self.ds.data[k.offs:k.offs + k.size]}', 4)
            self.ds.seek(k.offs + k.size)
            # fixme: handle missing value (">>" immediately after the key) - undocumented
            # see sample: 1471f2c34d40083b574c0fa0930cba9311aa532bf7207c009b0361b7b6db8bb7
            while 1:
                v = self.doc.parse_object()
                if v.objtype != 'comment':
                    break
            self.doc.dbg(f'VALUE: {v}', 4)
            d[k.value] = v
            if k.value == 'Type':
                self.Type = v.value
        self.value = d
        size = self.ds.next_pos - self.offs
        if not self.in_stream:
            self.doc.dict_by_offset[self.offs] = self
        self._decoded = -1  # not attempted marker
        return size


    @property
    def decoded(self):
        """
        Apply the filters (if any) and return the decoded object data.

        :return: The decoded data, None on failure / no stream to decode.
        """
        if self._decoded is not -1:
            return self._decoded
        if 'Filter' not in self.value:
            return None
        filters = self['Filter'] if self['Filter'].objtype == 'array' else [self['Filter']]
        data = getattr(self, 'stream')
        if data is None:
            self._decoded = data
            return None
        if self.doc.encrypted:
            # stream data is encrypted, must decrypt first
            self.doc.warn('Cannot decode stream: document is encrypted (to be implemented)!',
                    once=True)  # todo
            return None
        for f in filters:
            f = flt.ABBREVIATIONS.get(f.value, f.value)
            decoder = getattr(flt, f, None)
            if not decoder:
                self.doc.warn(f'Cannot decode {f}!')
                data = None
                break
            self.doc.dbg(f'Decoding {len(data)} bytes ({data[:40]}...) as {f} @ obj. {self.oid}...',
                    2)
            try:
                data = decoder(data)
            except Exception as e:
                self.doc.warn(f'Failed decoding object {self.oid} with {f}: {e}')
                data = None
                break
        self._decoded = data
        return self._decoded


    def __contains__(self, k):
        return k in self.dvalue


    def __getitem__(self, k):
        return self.dvalue[k]


class POStream(PdfObj):
    """
    Stream object.
    """


class POReference(PdfObj):
    """
    Reference to an object.
    """

    def __getitem__(self, k):
        return self.doc.objects[self.value].dvalue[k]


class PEComment(PdfObj):
    """
    Comment.
    """


class PEXref(PdfObj):
    """
    XREF section.
    """


class PEStartXref(PdfObj):
    """
    XREF section pointer.
    """


class PETrailer(PdfObj):
    """
    Trailer section.
    """

    def parse(self):
        self.value = self.doc.parse_object()
        self.doc.root_id = int(self.value.value['Root'].value)
        if 'Encrypt' in self.value:
            self.doc.encrypted = True
        return self.ds.next_pos - self.offs



class PdfDocument:
    """
    PDF document.

    :param from_magic: If set, parse sequentially from magic number. If False, start from XREF.
    :param debug: Enable debug messages (set to 2, 3 or 4 to be more verbose).
    :param warnings: Enable warning messages.
    """

    def __init__(self, filename=None, data=None, from_magic=True, debug=False, warnings=True,
            parse=False, logical_parse=False):
        self.ds = DataSource(filename, data)
        self.from_magic = from_magic
        self.offsets = {}
        self.root_id = self.xref = self.xrefptr = self.trailer = None
        self.objects = {}
        self.objlst = []
        self.dict_by_offset = {}
        self.debug = int(debug)
        self.warnings = warnings
        self.shown_warnings = set()
        self.encrypted = False
        if parse:
            self.parse()
            if logical_parse:
                self.parse_logical()


    def dbg(self, msg, level=1):
        """
        Print debug message (if enabled).
        """
        if level <= self.debug:
            sys.stderr.write(f'[#] {msg}\n')


    def warn(self, msg, once=False):
        """
        Print warning message (if enabled).
        """
        if self.warnings:
            if once:
                if msg in self.shown_warnings:
                    return
                self.shown_warnings.add(msg)
            sys.stderr.write(f'[!] {msg}\n')


    def parse(self):
        """
        Parse document; return self.
        """
        m = RX['magic'].search(self.ds[:1024])
        if m is None:
            raise ValueError('File is missing magic marker!')
        self.offsets['magic'] = m.start()
        PEComment(self, m.start(), value=m.group(), desc='magic', size=len(m.group()), toplevel=True)
        self.ver = m.groups()[0]
        if self.from_magic:
            self.parse_toplevel_objects(offs=m.end())
        else:
            self.parse_from_xref()
        return self


    def goto_next_char(self, always=False):
        """
        Move the cursor to the first non-whitespace character.

        :param always: If set, skips the current character even if it's not whitespace.
        """
        if (self.ds.last is not None and self.ds.last in WHITESPACE) or always:
            while self.ds.next() is not None and self.ds.last in WHITESPACE:
                pass


    def parse_toplevel_objects(self, offs=0, count=None):
        """
        Parse objects sequentially.
        """
        cnt = 0
        self.ds.seek(offs)
        while 1:
            obj = self.parse_object()
            if obj is None:
                break
            obj.toplevel = True
            self.dbg('Finished TOPLEVEL object', 4)
            cnt += 1
            if isinstance(obj, PEComment) and obj.value.startswith(EOF):
                # todo: handle linearized PDFs (first %%EOF is for the parameter dictionary)
                break
            if count is not None and cnt >= count:
                break
        return cnt


    def parse_object(self, obj_offs=None, oid=None, gen=None, in_stream=None):
        """
        Parse the object at the current position.
        """
        # fixme: ensure comments in the middle of tokens are handled correctly
        self.goto_next_char(True)
        if self.ds.last is None:
            return None
        offs = self.ds.next_pos - 1
        obj_offs = obj_offs or offs
        szdelta = offs - obj_offs
        crt = self.ds.last
        self.dbg(f'PARSING @{offs}; C:{crt}; buffer: {self.ds.data[offs:offs + 10]}...; next:'
                f'{self.ds.next_pos}', 3)
        oargs = {'doc': self, 'offs': obj_offs, 'in_stream': in_stream, 'oid': oid, 'gen': gen}
        if crt == b'%':
            # comment
            comment = self.ds.readline()
            if comment is None:
                # no LF at EOF
                comment = self.ds.read(self.ds.next_pos)
            obj = PEComment(value=comment.strip(), size=szdelta + len(comment) + 1, **oargs)
        elif crt.isdigit() or crt in b'-+.':
            # object or number
            m = RX['id'].match(self.ds.data, pos=offs)
            if m:
                oid, gen, otype = m.groups()
                if otype == b'R':
                    # reference to object
                    obj = POReference(value=int(oid), size=szdelta + len(m.group()), **oargs)
                else:
                    # fixme: add a POIndirectObject type? move code from here.
                    self.ds.seek(offs + len(m.group()))
                    while 1:
                        obj = self.parse_object(obj_offs=offs, oid=oid, gen=gen)
                        if obj.objtype != 'comment':
                            break
                    self.goto_next_char(True)
                    if self.ds.last == b's' and self.ds.peek(5) == b'tream':
                        if 'Length' not in obj.value:
                            self.warn(f'Stream dict without Length key @ {offs}!')
                            length = None
                        else:
                            length = obj.value['Length'].value
                            if obj.value['Length'].objtype == 'reference':
                                if length not in self.objects:
                                    length = None
                                else:
                                    length = self.objects[length].value
                        m = RX['stream'].match(self.ds.data, pos=self.ds.next_pos - 1)
                        if not m:
                            raise ValueError('Missing endstream!')
                        ssize = len(m.group())
                        if length is not None and abs(ssize - 19 - length) > 2:
                            self.warn(f'Mismatched stream length: {length} vs. {ssize - 19} detected!')
                        obj.size += ssize
                        obj.stream = m.groups()[0]
                        self.ds.seek(offs + obj.size)
                        self.dbg(f'STREAM: {obj.stream}', 2)
                        self.goto_next_char(True)
                    if self.ds.last == b'e' and self.ds.peek(5) == b'ndobj':
                        self.dbg('ENDOBJ', 3)
                        obj.set_size(self.ds.next_pos + 5 - offs)
                    else:
                        self.warn(f'Missing endobj @ {self.ds.next_pos - 1}: '
                                f'{self.ds.last + self.ds.peek(20)}!')
            else:
                m = RX['number'].match(self.ds.data, pos=offs)
                # crt is a digit, it must match
                obj = PONumber(value=ast.literal_eval(m.group().decode('utf8')),
                        size=szdelta + len(m.group()), **oargs)
        elif crt == b'[':
            # list
            obj = POArray(**oargs)
        elif crt == b'<':
            if self.ds.next() != b'<':
                # hex string
                obj = POString(**oargs)
            else:
                # dict
                obj = PODictionary(**oargs)
        elif crt == b'/':
            # name
            obj = POName(**oargs)
        elif crt in b'tf' and RX['bool'].match(self.ds.data, pos=offs):
            obj = POBoolean(value=crt == b't', size=szdelta + (4 if crt == b't' else 5), **oargs)
        elif crt == b'n' and self.ds.peek(3) == b'ull':
            obj = PONull(size=szdelta + 4, **oargs)
        elif crt == b'(':
            obj = POString(**oargs)
        elif crt == b't' and self.ds.peek(6) == b'railer':
            # fixme: improve trailer, xref, startxref, boolean and null handling
            self.ds.seek(offs + 7)
            obj = PETrailer(**oargs)
        elif crt == b'x' and self.ds.peek(3) == b'ref':
            # fixme: move code out
            self.ds.seek(offs + 4)
            xref = {}
            while 1:
                self.goto_next_char(True)
                if not self.ds.last.isdigit():
                    self.ds.back()
                    break
                line = self.ds.last + self.ds.readline().rstrip()
                line = tuple(line.decode('utf8').split(' '))
                if len(line) == 2:
                    crt = xref[line] = []
                elif len(line) == 3:
                    crt.append(line)
                else:
                    raise ValueError(f'Invalid xref line: {line}!')
            obj = self.xref = PEXref(value=xref, size=self.ds.next_pos - offs, **oargs)
        elif crt == b's' and self.ds.peek(8) == b'tartxref':
            self.ds.seek(offs + 9)
            n = self.parse_object()
            obj = self.xrefptr = PEStartXref(value=n, size=self.ds.next_pos - offs, **oargs)
        else:
            raise ValueError(f'Invalid character {self.ds.last} at offset {offs}: '
                    f'{self.ds.data[offs:offs + 30]}...!')
        self.ds.seek(obj.offs + obj.size)
        if self.debug >= 2:
            odata = obj.data
            if len(odata) > 40:
                odata = odata[:20] + b'...' + odata[-20:]
            self.dbg(f'OBJ: {obj}; data: {odata} ({obj.size}); next: {self.ds.peek(1)}', 2)
        return obj


    def parse_object_streams(self):
        """
        Parse object streams (ObjStm dictionaries).
        """
        docds = self.ds
        for d in list(self.dict_by_offset.values()):
            if d.Type == 'ObjStm':
                data = d.decoded
                if data is not None:
                    # parse objects in object stream
                    objoffs = d['First'].value
                    d.decoded_meta_data = data[:objoffs]
                    d.decoded_obj_data = data[objoffs:]
                    self.ds = DataSource(f':ObjStm-{d.oid}:', d.decoded_obj_data)
                    metapos = 0
                    for i in range(d['N'].value):
                        m = RX['intpair'].search(d.decoded_meta_data, pos=metapos)
                        metapos = m.end()
                        oid, offs = [int(x) for x in m.groups()]
                        self.ds.seek(offs)
                        self.parse_object(oid=oid, gen=0, in_stream=d.oid)
        self.ds = docds


    def parse_logical(self):
        """
        Parse the logical structure of the document (disable this for malware analysis).
        """
        self.parse_object_streams()
        if self.root_id:
            self.root = self.objects[self.root_id]
        elif self.xrefptr and self.xrefptr.value.value:
            self.root = self.dict_by_offset[self.xrefptr.value.value]['Root'].dvalue
        else:
            raise ValueError('Can\'t find Catalog!')
        self.dbg(f'ROOT: {self.root}')
        assert(self.root['Type'].value == 'Catalog')
        # self.outlines = self.root['Outlines'].dvalue if 'Outlines' in self.root else None
        self.pagetree = self.root['Pages']
        self.pages = []
        q = [self.pagetree.dereferenced]
        while q:
            crt = q.pop(0)
            if crt.Type == 'Pages':
                q += list([e.dereferenced for e in crt['Kids'].dvalue])
            else:
                while crt.objtype == 'reference':
                    crt = self.objects[crt.value]
                self.pages.append(crt)
        self.dbg(f'Found {len(self.pages)} pages.')
        for i, p in enumerate(self.pages):
            contents = p['Contents']
            if type(contents) is not list:
                contents = [contents]
            p.page_data = b''
            for e in contents:
                p.page_data += self.objects[e.value].decoded
            p.number = i + 1



def tokenize(text):
    """
    Tokenize page content.
    """
    if type(text) is bytes:
        text = text.decode('utf8')
    prev = 0
    for m in RX['tokenizer'].finditer(text):
        if m.start() > prev:
            t = text[prev:m.start()]
            if RX['is-number'].search(t):
                t = ast.literal_eval(t)
            yield t
        t = m.group()
        if t.strip():
            if t[0] == '(':
                t = RX['octals'].sub(lambda x:chr(int(x.groups()[0], 8)), t)
            elif t[0] == '<' and t[1] != '<':
                t = binascii.unhexlify(t[1:-1])
            yield t
        prev = m.end()
    if prev < len(text):
        yield text[prev:]


def group_tokenize(text):
    """
    Tokenize into groups ending in operators.
    """
    tokens = tokenize(text)
    crt = []
    for t in tokens:
        crt.append(t)
        if type(t) is str and t not in TEXT_DELIMITERS and t[0] not in '/(':
            yield crt
            crt = []
    if crt:
        yield crt


