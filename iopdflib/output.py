"""
iopdf: Output generation.

Author: Vlad Topan (vtopan/gmail)
"""

from .parsepdf import PdfDocument, group_tokenize

import textwrap


HTML_TEMPLATE = '''<html>
  <head>
    <title>Document (TODO)</title>
    <style>
      div.page {
        position: relative;
        top: 0;
        left: 0;
        border: 1px solid #000;
      }
    </style>
  </head>
<body>'''


def raw_objects(document, decode=True, toplevel=True):
    """
    Extract all raw objects (decoded where relevant) from the document.

    :param decode: Decode encoded streams (prefix 'stream' with '(decoded)' in output to mark them).
    :param toplevel: Only dump toplevel objects.
    """
    if type(document) is str:
        document = PdfDocument(document, parse=True)
    for o in document.objlst:
        if o.in_stream or (toplevel and not o.toplevel):
            continue
        data = o.data
        if decode and o.objtype == 'dictionary':
            decoded = o.decoded
            if decoded is not None:
                data = data[:data.find(b'stream')] + b'(decoded)stream\n' + decoded \
                    + b'\nendstream\nendobj'
        yield data


def raw_text_data(document):
    """
    Extract raw text data from the document.
    """
    if type(document) is str:
        document = PdfDocument(document, parse=True)
    document.parse_logical()
    res = []
    for p in document.pages:
        res.append(f'### Page {p.number}')
        for grp in group_tokenize(p.page_data):
            res.append(' '.join(str(x) for x in grp))
        res.append('')
    return '\n'.join(res)


def extract_text_chunks(document):
    """
    Extract the text from the document, returning each chunk iteratively.

    `chunk_type` is one of: 'p'(aragraph), 'h1', 'h2', ..., 'h5', 'binary', 'new_page', 'end_page'.
    `chunk` variable type is `str` when chunk_type in ('p', 'h*'), `bytes` for 'binary', `int`
    for 'new_page' (containing page number).

    :return: tuple (chunk, chunk_type)
    """
    if type(document) is str:
        document = PdfDocument(document, parse=True)
    document.parse_logical()
    # dry run - try to guess element types
    heading_sizes = set()
    for p in document.pages[:5]:
        for grp in group_tokenize(p.page_data):
            if grp[-1] == 'Tf':
                if grp[1] >= 12:
                    heading_sizes.add(grp[1])
    heading_sizes = sorted(heading_sizes, reverse=1)[:5]
    # actual run
    for i, p in enumerate(document.pages):
        yield (i + 1, 'new_page')
        in_text = 0
        for grp in group_tokenize(p.page_data):
            if grp[0] == 'BT':
                in_text = 1
                x, y = 0, 0
                text = []
                tag = 'p'
            elif grp[0] == 'ET':
                in_text = 0
                if text:
                    yield ("".join(text), tag)
            elif in_text:
                if grp[-1] == 'Tf':
                    if text:
                        yield ("".join(text), tag)
                        text = []
                    if grp[1] in heading_sizes:
                        tag = f'h{heading_sizes.index(grp[1]) + 1}'
                    else:
                        tag = 'p'
                    # todo: font
                    pass
                elif grp[-1] in ('TD', 'Td'):
                    x, y = x + int(grp[0]), y + int(grp[1])
                    if text and abs(grp[1]) > 20:
                        # new paragraph
                        yield ("".join(text), tag)
                        text = []
                elif grp[-1] in ('Tj', 'TJ'):
                    tokens = grp[0:1] if grp[-1] == 'Tj' else grp[1:-2]
                    crt_text = []
                    for t in tokens:
                        if type(t) is str:
                            if t[0] != '(' or t[-1] != ')':
                                raise ValueError(f'Unexpected token {t}!')
                            crt_text.append(t[1:-1])
                            continue
                        elif type(t) is bytes:
                            crt_text.append(t.decode('utf8', errors='replace'))
                            continue
                        if -200 >= t >= -1000:
                            crt_text.append(' ')
                        elif t < -1000:
                            # indented text, todo...
                            crt_text.append('\t')
                    crt_text = ''.join(crt_text)
                    if tag == 'p':
                        if len(crt_text) < 79:
                            yield ("".join(text), tag)
                            text = []
                    text.append(crt_text)
        yield (None, 'end_page')


def convert_to_html(document):
    """
    Converts a PDF document (as filename or PdfDocument) to HTML.
    """
    res = []
    res.append(HTML_TEMPLATE)
    for chunk, ctype in extract_text_chunks(document):
        if ctype == 'new_page':
            res.append('    <div class="page">')
        elif ctype == 'end_page':
            res.append('    </div>')
        elif ctype == 'p' or ctype[0] == 'h':
            res.append(f'<{ctype}>{chunk}</{ctype}>')
    res.append('  </body>\n</html>\n')
    return '\n'.join(res)


def convert_to_text(document, width=100):
    """
    Converts a PDF document (as filename or PdfDocument) to text (Markdown).
    """
    res = []
    prev = None
    for chunk, ctype in extract_text_chunks(document):
        if ctype == 'new_page':
            pass
        elif ctype == 'end_page':
            res.append('\n')
        elif ctype == 'p':
            if chunk.strip():
                res.append('\n'.join(textwrap.wrap(chunk, width=width)) + '\n')
        elif ctype[0] == 'h':
            if prev == ctype:
                res[-1] = res[-1].rstrip() + ' ' + chunk + '\n'
            else:
                res.append('#' * int(ctype[1]) + ' ' + chunk + '\n')
        prev = ctype
    return '\n'.join(res)

