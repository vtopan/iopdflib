#!/usr/bin/env python3
"""
Creates a symlink to iopdf in site-packages.

Author: Vlad Topan (vtopan/gmail).
"""

import os
import platform
import sys


plat = platform.system()
if plat == 'Windows':
    path = f'{os.path.dirname(sys.executable)}\\lib\\site-packages'
elif plat == 'Linux':
    path = os.path.expanduser('~/.local/lib/python%s.%s/site-packages' % sys.version_info[:2])
else:
    raise ValueError(f'Unknown platform {plat}!')
if not os.path.isdir(path):
    os.makedirs(path)
iopdf_path = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
os.symlink(os.path.join(iopdf_path, 'iopdflib'), os.path.join(path, 'iopdflib'))
