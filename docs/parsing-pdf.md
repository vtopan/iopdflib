# Parsing the PDF File Format

- file format standardized as ISO 32000, but Adobe's Reader is the de facto reference
  implementation (including quirks)


## File Structure

- ASCII text for almost all content types (exception: encoded streams)
- bird's eye view of elements, in order:
    - a header magic *marker* (`%PDF-`) followed by the PDF Spec version (`1.0` .. `1.7` 
      as of Mar. 2019)
    - a sequence of *objects* in the form `<id> <generation> <object>`
        - `id` (number) and `generation` are integers
    - the *XREF table* (references all objects in the document)
        - sequence of *subsections* (two numbers) followed by one or more pointers 
          (format: `offset generation f|n`)
    - the *trailer*, which contains document statistics followed by a pointer to the XREF
      (`startxref`) and ends with `%%EOF`


### Objects

- Boolean values
- Integer and real numbers
- Strings
- Names
- Arrays
- Dictionaries
- Streams
- The null object
        
        
## Where to start

- the XREF table at the end (pointed to by the trailer) provides exact offsets, but it may be absent
- the header/magic (`%PDF-1.`) can be anywhere in the first KB of the file


## Anomalies accepted by the Adobe Reader

- the magic anywhere in the first KB of the file
- missing XREF section


## References

- [InfoSec - PDF File Format: Basic Structure (2018)](
    https://resources.infosecinstitute.com/pdf-file-format-basic-structure/)
- [Adobe - PDF Reference, 6th ed. (2006)](        
    https://www.adobe.com/content/dam/acom/en/devnet/pdf/pdf_reference_archive/pdf_reference_1-7.pdf)
- [Sandia - Deep PDF Parsing to Extract Features for Detecting Embedded Malware 
    (2011)](https://prod-ng.sandia.gov/techlib-noauth/access-control.cgi/2011/117982.pdf)

