#!/usr/bin/env python3
"""
iopdf: PDF processing tool (text extraction, conversion to other formats, malware analysis, etc.).

Author: Vlad Topan (vtopan/gmail)
"""
import argparse
import glob
import gzip
import sys

import iopdflib as ipl


__version__ = '0.1.0'


if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='iopdf', description=__doc__,
            formatter_class=argparse.RawDescriptionHelpFormatter)
    parser.add_argument('filenames', help='the data source filename(s)', nargs='+')
    ops = parser.add_mutually_exclusive_group(required=True)
    ops.add_argument('-T', '--extract-text', help='extract text',
            dest='op', action='store_const', const='extract_text', default='extract_text')
    ops.add_argument('-DR', '--dump-raw', help='dump all raw objects (decoded where relevant)',
            dest='op', action='store_const', const='dump_raw', default='dump_raw')
    ops.add_argument('-DT', '--dump-text', help='dump raw text data',
            dest='op', action='store_const', const='dump_text', default='dump_text')
    ops.add_argument('-CH', '--conv-html', help='convert to HTML',
            dest='op', action='store_const', const='conv_html', default='conv_html')
    parser.add_argument('-d', '--debug', help='print debug messages', action='count', default=0)
    parser.add_argument('-o', '--output', help='output filename, can end with .gz (default: stdout)',
            default='-')
    args = parser.parse_args()
    if args.output != '-':
        if '%s' not in args.output:
            if args.output.endswith('.gz'):
                sys.stdout = gzip.open(args.output, 'wt')
            else:
                if args.op in ('dump_raw',):
                    sys.stdout = open(args.output, 'wb')
                else:
                    sys.stdout = open(args.output, 'w', encoding='utf8')
    elif args.op in ('dump_raw',):
        sys.stdout = sys.stdout.buffer

    filenames = [x for f in args.filenames for x in glob.glob(f)]
    if not filenames:
        sys.stderr.write('[!] No files given!')
        sys.exit()
    for filename in filenames:
        if len(filenames) > 1:
            sys.stderr.write(f'[*] Processing {filename}...\n')
        if '%s' in args.output:
            ofn = args.output.replace('%s', filename)
            if args.output.endswith('.gz'):
                sys.stdout = gzip.open(ofn, 'wt')
            else:
                if args.op in ('dump_raw',):
                    sys.stdout = open(ofn, 'wb')
                else:
                    sys.stdout = open(ofn, 'w', encoding='utf8')
        doc = ipl.PdfDocument(filename, debug=args.debug, parse=1)
        if args.op == 'conv_html':
            print(ipl.convert_to_html(doc))
        elif args.op == 'extract_text':
            print(ipl.convert_to_text(doc))
        elif args.op == 'dump_raw':
            sys.stdout.write(b'\n'.join(ipl.raw_objects(doc)) + b'\n')
        elif args.op == 'dump_text':
            print(ipl.raw_text_data(doc))

